package com.work.Two.firstSection.test;

import com.work.Two.firstSection.main.Token.TokenType;
import com.work.Two.firstSection.main.TokenStream;
import com.work.Two.firstSection.main.YourTokenStream;
import com.work.Two.threeSection.Stack;

public class TokenTest {
	public static void main(String[] args) throws Exception {
		Stack<Integer> stack = new Stack<>(100);
		TokenStream stream = new YourTokenStream(System.in);
		while(stream.geToken().tokenType!=TokenType.NONE){
			if(stream.geToken().tokenType == TokenType.INT){
				stack.push((Integer)stream.geToken().value);
			}else if(stream.geToken().tokenType == TokenType.PLUS){
				int a = stack.pop();
				int b = stack.pop();
				stack.push(a+b);
			}else if(stream.geToken().tokenType == TokenType.MINUS){
				int a = stack.pop();
				int b = stack.pop();
				stack.push(a-b);
			}else if(stream.geToken().tokenType == TokenType.MULT){
				int a = stack.pop();
				int b = stack.pop();
				stack.push(a*b);
			}else if(stream.geToken().tokenType == TokenType.DIV){
				int a = stack.pop();
				int b = stack.pop();
				stack.push(a/b);
			}
			stream.consumeToken();
		}
		System.out.println(stack.pop());
	}
}