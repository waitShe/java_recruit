package com.work.Two.firstSection.test;

public class Mian {
	public static void main(String[] args) {
		Shape shape = new Circle();
		shape.draw();
	}
}
abstract class Shape{
	public void draw(){
		System.out.println("draw Shape");
	}
	
	public abstract double area();
}

class Circle extends Shape{
	public final void draw(){
		System.out.println("draw Circle");
	}

	@Override
	public double area() {
		return 3.14;
	}
	
}